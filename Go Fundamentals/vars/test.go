package main

// importing fmt for formatting strings and printing them to the console
import "fmt"

// there must be one and only one main() func 
// if main() is not there GO will not call the function name

func main() {
	fmt.Println("Hello, this was written in Go!")

	// string variables
	var nameOne string = "mario"
	var nameTwo = "luigi"
	var nameThree string
	nameThree = "bowser"
	fmt.Println(nameOne, nameTwo, nameThree)

	nameFour := "yoshi"
	fmt.Println(nameFour)

	// integer variables
	var ageOne int = 20
	var agetwo = 30
	agethree := 40

	fmt.Println(ageOne, agetwo, agethree)

	isBool := false
	fmt.Println(isBool)
}
